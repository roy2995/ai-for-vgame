using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_last : MonoBehaviour
{
    [Header("Evadir & Flee variable")]
    private Vector2 characterVelocity = Vector2.zero;
    [SerializeField] private Rigidbody2D player;
    [SerializeField] private float frameAhead, fleeSpeed;

    [Header("patrol variable")]
    public float cronometro;
    public int direccion;
    public int angulo;
    public float patrolSpeed;
    private int Active;

    [Header("pursuit variable")]
    public Transform Objetive;
    public float pursuitSpeed;

    [Header("Arrive")]
    public GameObject RayOrigin;
    public GameObject Target;
    public GameObject Guard;
    public GameObject PlayerPosition;
    public float distance;
    public float Aspeed = 10f;
    public float positionspeed = 500000000;
    Vector3 NewPosition;
    RaycastHit2D hit;
    bool lastknown = false;
    Vector3 NewDirection;
    public GameObject RayDirection;

    [Header("Control Funtion")]
    public int rutina;
    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Q))
        {
            rutina++;
        }

        switch (rutina)
        {
            case 0:
                cronometro += 1 * Time.deltaTime;
                if (cronometro >= 5)
                {
                    direccion = Random.Range(0, 2);
                    angulo = Random.Range(-90, 90);
                    //Active = true;
                }
                switch (direccion)
                {
                    case 0:
                        transform.rotation = Quaternion.Euler(0, 0, angulo);
                        transform.Translate(Vector3.right * patrolSpeed * Time.deltaTime);
                        cronometro = 0;
                        //Active = false
                        break;
                    case 1:
                        transform.rotation = Quaternion.Euler(0, 180, angulo);
                        transform.Translate(Vector3.right * patrolSpeed * Time.deltaTime);
                        //Active = false;
                        cronometro = 0;
                        break;
                }
                break;

            case 1://david

                if (Objetive == null)
                    return;
                transform.position = Vector2.MoveTowards(transform.position, Objetive.position, pursuitSpeed * Time.deltaTime);
                break;

            case 2:
                Flee(player.position);
                break;
            case 3:
                Evade(player, player.position);
                break;
            case 4:

                if (lastknown == false)
                {
                    NewDirection = RayDirection.transform.position;
                    hit = Physics2D.Raycast(RayOrigin.transform.position, NewDirection, distance);
                    Debug.DrawRay(RayOrigin.transform.position, NewDirection * distance, Color.blue);
                    if (hit.collider != null)
                    {
                        Target.transform.position = Vector3.MoveTowards(PlayerPosition.transform.position, PlayerPosition.transform.position, positionspeed * Time.deltaTime);
                        Debug.Log("jugador encontrado");
                        lastknown = true;
                    }
                }
                if (lastknown == true)
                {
                    Debug.Log("investigando");
                    Guard.transform.position = Vector3.MoveTowards(Guard.transform.position, Target.transform.position, Aspeed * Time.deltaTime);
                    if(Guard.transform.position == Target.transform.position)
                    {
                        lastknown = false;
                    }
                }
                break;
        }
        if(rutina == 5)
        {
            rutina = 0;
        }
    }

    private void Flee(Vector2 playerPosition)
    {
        Vector2 distance = (Vector2)transform.position - playerPosition;

        Vector2 desiredVelocity = distance.normalized * fleeSpeed;

        Vector2 steeringForce = desiredVelocity - characterVelocity;
        characterVelocity += steeringForce * Time.deltaTime;
        transform.position += (Vector3)characterVelocity * Time.deltaTime;
    }

    private void Evade(Rigidbody2D player, Vector2 playerPosition)
    {
        float framesAhead = 6;

        Vector2 target = playerPosition;

        Vector2 prediction = player.velocity * framesAhead;

        target += prediction;

        Flee(target);
    }

    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            pursuitSpeed = -2;

            Debug.Log("Character reach min distance");
        }
    }*/

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log(collision);
        if (collision.CompareTag("Player"))
        {
            pursuitSpeed = 2;
            transform.up = Objetive.position - transform.position;

            Debug.Log("Character is chasing");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            pursuitSpeed = 0;

            Debug.Log("Player has flea");
        }
    }
}
